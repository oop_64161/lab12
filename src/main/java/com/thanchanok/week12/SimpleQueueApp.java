package com.thanchanok.week12;

import java.util.LinkedList;
import java.util.Scanner;

public class SimpleQueueApp {
    private static Scanner  sc = new Scanner(System.in);
    private static int menu=0;
    private static LinkedList<String> queue = new LinkedList();
    private static String current;
    public static void main(String[] args) {
        while(true) {
            showQueue();
            printMenu();
            InputMenu();
        }
    }
    

    private static void showQueue() {
        System.out.println(queue);
    }


    private static void InputMenu() {
        try{
            System.out.print("Plase input menu (1-3): ");
            menu = sc.nextInt();
            switch(menu){
                case 1:
                    NewQueue();
                    break;
                case 2:
                    GetQueue();
                    break;
                case 3:
                    exit();
                    break;
                default:
                    System.out.println("Plase input menu (1-3)");
            }
        }catch(Exception e){
            System.out.println("Error: Please input 1-3");
            sc.next();
        }
    }

    private static void GetQueue() {
        if(queue.isEmpty()){
            System.out.println("Queue Empty");
            return;
        }
        current = queue.remove();
        System.out.println("Current: " + queue.remove());
    }


    private static void exit() {
        System.out.println("Bye Bye!!!");
        System.exit(0);
    }


    private static void NewQueue() {
        System.out.print("Please input name: ");
        String name = sc.next();
        queue.add(name);
    }

    private static void printMenu() {
        System.out.println("----Menu----");
        System.out.println("1. New Queue");
        System.out.println("2. Get Queue");
        System.out.println("3. Exit");
        System.out.println("------------");
    }
}
